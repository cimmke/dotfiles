" .vimrc settings

set hidden

" Indent based on filetype
filetype indent plugin on

" Syntax highlighting
syntax on

" Allow backspace in insert mode
set backspace=indent,eol,start

" Show incomplete commands in the bottom
set showcmd

" Show current mode in bottom
set showmode

" Turn annoying sound off
set visualbell

" View tab completion options
set wildmenu

" Add line numbers
set number

" Know where in buffer you are
set ruler

" Ignore case when searching
set ignorecase

" Keep same indent as previous line if no filetype
set autoindent

" Use 4 spaces instead of tabs
set shiftwidth=4
set softtabstop=4
set expandtab

" Remap h, j, k, and l so movements are in natural position
noremap ; l
noremap l k
noremap k j
noremap j h

" Show matching bracket
set showmatch
